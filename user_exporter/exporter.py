#!/usr/bin/python
# -*- coding: UTF-8 -*-

#import required modules
import os
import sys
import requests
import time
import json

#import the prometheus modules which we need
import prometheus_client
from prometheus_client import start_http_server, Metric, REGISTRY


class JsonCollector(object):
    def __init__(self):
        pass

    def collect(self):
		
	#define the host of the stackstorm api
        mastodon_host = os.environ.get('MASTODON_HOST', 'https://diaspodon.fr/')
		
	#strip the / off the end of the hostname, we will add that ourselves
        mastodon_host_url = mastodon_host.rstrip("/")
		
        url = mastodon_host + 'api/v1/accounts/1'
        r = requests.get(url)
        data = json.loads(r.content.decode())
        metric = Metric('following_count', 'Number of following account', 'gauge')
        metric.add_sample('following_count', value=data["following_count"], labels={})
#        print (data["display_name"])
#        print (data["following_count"])
#        print (data["followers_count"])
        yield metric

        url = mastodon_host + 'api/v1/accounts/1'
        r = requests.get(url)
        data = json.loads(r.content.decode())
        metric = Metric('followers_count', 'Number of followers', 'gauge')
        metric.add_sample('followers_count', value=data["followers_count"], labels={})
        yield metric

        url = mastodon_host + 'api/v1/accounts/1'
        r = requests.get(url)
        data = json.loads(r.content.decode())
        metric = Metric('statuses_count', 'Number of statuses', 'gauge')
        metric.add_sample('statuses_count', value=data["statuses_count"], labels={})
        yield metric

if __name__ == "__main__":
	#start the webserver on the required port
    start_http_server(9400)
    REGISTRY.register(JsonCollector())
    while True: time.sleep(1)
