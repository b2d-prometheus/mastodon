### Instance Exporter

Uses instance URL to fetch informations

- Number of users
- Number of known domains
- Number of logins
- Number of active users
- Number of toots
- Number of registrations
- Mastodon version

### User Exporter

Uses a user ID to fetch informations

- Number of followers
- Number of following
- Number of toots


## Installation

```
pip3 install -r requirements.txt'
```

## Docker

- Build 

```
docker build -t mastodon-exporter .
```

- Run 

```
docker run -d -e MASTODON_HOST="https://instance.url/" -p 9410:9410 mastodon-exporter
```
