### Instance Exporter - WIP

- Number of users : instance_user_count
- Number of known domains : instance_domain_count
- Number of weekly logins : instance_week_login
- Number of weekly toots : instance_week_toots
- Number of toots : instance_status_count
- Number of registrations : instance_week_registration


## Installation

- Installation

```
pip3 install -r requirements.txt
```

- Run

Edit MASTODON_HOST to fetch your own instance

```
python3 instance_exporter.py
```

## Docker

- Build 

```
docker build -t mastodon-exporter .
```

- Run 

```
docker run -d -e MASTODON_HOST="https://instance.url/" -p 9410:9410 mastodon-exporter
```
