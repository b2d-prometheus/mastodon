#!/usr/bin/python

import os
import sys
import requests
import time
import json

import prometheus_client
from prometheus_client import start_http_server, Metric, REGISTRY


class JsonCollector(object):
    def __init__(self):
        pass

    def collect(self):
		
        mastodon_host = os.environ.get('MASTODON_HOST', 'https://diaspodon.fr/')
		
        mastodon_host_url = mastodon_host.rstrip("/")
		
        url = mastodon_host + 'api/v1/instance'
        r = requests.get(url)
        data = json.loads(r.content.decode())
        metric = Metric('instance_user_count', 'User accounts', 'gauge')
        metric.add_sample('instance_user_count', value=float(data["stats"]["user_count"]), labels={})
        max_user = data["stats"]["user_count"]
        yield metric

        url = mastodon_host + 'api/v1/instance'
        r = requests.get(url)
        data = json.loads(r.content.decode())
        metric = Metric('instance_status_count', 'Number of statuses', 'gauge')
        metric.add_sample('instance_status_count', value=float(data["stats"]["status_count"]), labels={})
        yield metric

        url = mastodon_host + 'api/v1/instance'
        r = requests.get(url)
        data = json.loads(r.content.decode())
        metric = Metric('instance_domain_count', 'Number of known domains', 'gauge')
        metric.add_sample('instance_domain_count', value=float(data["stats"]["domain_count"]), labels={})
        yield metric

        url = mastodon_host + 'api/v1/instance'
        r = requests.get(url)
        data = json.loads(r.content.decode())
        metric = Metric('instance_version', 'Mastodon Version', 'gauge')
        version = data["version"].split(".")[0] + "." + data["version"].split(".")[1]
        metric.add_sample('instance_version', value=float(version), labels={})
        yield metric

        url = mastodon_host + 'api/v1/instance'
        r = requests.get(url)
        data = json.loads(r.content.decode())
        metric = Metric('instance_subversion', 'Mastodon Version', 'gauge')
        subversion = data["version"].split(".")[2] 
        metric.add_sample('instance_subversion', value=float(subversion), labels={})
        yield metric

        url = mastodon_host + 'api/v1/instance/activity'
        r = requests.get(url)
        li = json.loads(r.content.decode())
        metric = Metric('instance_week_login', 'Number of logins', 'gauge')
        metric.add_sample('instance_week_login', value=float(li[0]["logins"]), labels={})
        yield metric

        url = mastodon_host + 'api/v1/instance/activity'
        r = requests.get(url)
        li = json.loads(r.content.decode())
        metric = Metric('instance_week_registration', 'Number of registrations', 'gauge')
        metric.add_sample('instance_week_registrations', value=float(li[0]["registrations"]), labels={})
        yield metric

        url = mastodon_host + 'api/v1/instance/activity'
        r = requests.get(url)
        li = json.loads(r.content.decode())
        metric = Metric('instance_week_toots', 'Number of weekly toots', 'gauge')
        metric.add_sample('instance_week_toots', value=float(li[0]["statuses"]), labels={})
        yield metric

if __name__ == "__main__":
    start_http_server(9410)
    REGISTRY.register(JsonCollector())
    while True: time.sleep(1)
